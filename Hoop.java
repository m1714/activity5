package activity5;

public class Hoop implements Target{

    private double position;

    public Hoop (double position)
    {
        this.position = position;
    }

    public void receive(Projectile prj)
    {
        if (prj instanceof Basketball)
        {
            System.out.println("Got a basket!");
        }
        else
        {
            System.out.println("That wasn't a basket.");
        }
    }


    public double getPosition()
    {
        return this.position;
    }
    
}
